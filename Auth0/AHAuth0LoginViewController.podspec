#
#  Be sure to run `pod spec lint AHAuth0LoginViewController.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "AHAuth0LoginViewController"
  s.version      = "0.0.7"
  s.summary      = "A ViewController that can be used to authenticate with Auth0."
  s.description  = "A customizable view controller used to authenticate with Auth0. Style and Network settings can be configured using Protocols"
  s.homepage     = "https://bitbucket.org/dopedev/auth0/"
  s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    LICENSE
  }
  s.author       = { "Anthony Hoang" => "anthony.hoang720@gmail.com" }
  s.platform     = :ios, "9.3"
  s.source       = { :git => "https://bitbucket.org/dopedev/auth0/", :tag => "0.0.7" }
  s.exclude_files = "**/Auth0Tests/*.{h,m}", "**/Auth0UITests/*.{h,m}", "**/AppDelegate.{h,m}", "**/main.m" 
  s.source_files  = "Auth0", "Auth0/**/*.{h,m}"
  s.requires_arc = true
  s.dependency "AFNetworking", "~> 3.1.0"
  s.dependency "libextobjc", "~> 0.4.1"
  s.dependency "TPKeyboardAvoiding", "~> 1.3"
  s.dependency "Masonry", "~> 1.0.0"
  s.dependency "SVProgressHUD", "~> 2.0.3"
  s.frameworks = "Foundation", "UIKit"

end
