//
//  Auth0Tests.m
//  Auth0Tests
//
//  Created by Anthony Hoang on 4/21/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AHAuth0Client.h"

@interface Auth0Tests : XCTestCase

@end

@implementation Auth0Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

@end
