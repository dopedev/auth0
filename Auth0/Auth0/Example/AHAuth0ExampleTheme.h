//
//  AHAuth0ExampleTheme.h
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AHAuth0LoginTheme.h"

@interface AHAuth0ExampleTheme : NSObject <AHAuth0LoginTheme>

#pragma mark - Protocol
/**
 *  Sets the primary color of the views. The primary color will be used in views such as button backgrounds, navigation bars, ect.
 *
 *  @return The primary color of the views.
 */
- (UIColor *)primaryColor;
/**
 *  Sets the secondary color of the views. The secondary color will be used as the font color against the background(primary) color.
 *
 *  @return The secondary color.
 */
- (UIColor *)secondaryColor;
/**
 *  Sets the font of the labels and buttons.
 *
 *  @return The font name. Font size is not customizable.
 */
- (NSString *)fontName;
/**
 *  Sets the logo image at the top of the login page.
 *
 *  @return The logo image.
 */
- (UIImage *)logoImage;

@end
