//
//  AHAuth0ExampleTheme.m
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHAuth0ExampleTheme.h"

@implementation UIColor (HexColor)

+ (UIColor *)colorWithHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end

@implementation AHAuth0ExampleTheme

- (UIColor *)primaryColor {
    return [UIColor colorWithHexString:@"#2ecc71"];
}

- (UIColor *)secondaryColor {
    return [UIColor whiteColor];
}

- (NSString *)fontName {
    return @"Helvetica-Neue";
}

- (UIImage *)logoImage {
    return [UIImage imageNamed:@"auth0-logo-blue"];
}

@end
