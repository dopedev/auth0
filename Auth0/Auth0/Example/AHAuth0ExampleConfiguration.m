//
//  AHAuth0ExampleConfiguration.m
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHAuth0ExampleConfiguration.h"

static NSString * const kBaseUrl = @"https://anthonyhoang.auth0.com/";
static NSString * const kClientId = @"oZ8rO2N8cCcfZdES2gxTHbzDm5ynqAJZ";
static NSString * const kDatabaseName = @"Username-Password-Authentication";

@implementation AHAuth0ExampleConfiguration

- (NSString *)baseURL {
    return kBaseUrl;
}

- (NSString *)clientId {
    return kClientId;
}

- (NSString *)databaseName {
    return kDatabaseName;
}

@end
