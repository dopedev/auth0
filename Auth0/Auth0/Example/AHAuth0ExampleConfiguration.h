//
//  AHAuth0ExampleConfiguration.h
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AHAuth0NetworkConfiguration.h"

@interface AHAuth0ExampleConfiguration : NSObject <AHAuth0NetworkConfiguration>

#pragma mark - Protocol
/**
 *  The base URL used for Auth0
 *
 *  @return The URL used for Auth0
 */
- (NSString *)baseURL;
/**
 *  The client id used for requuest.
 *
 *  @return The client Id.
 */
- (NSString *)clientId;
/**
 *  The database name in your Auth0 account used for email/password authentication.
 *
 *  @return The database name.
 */
- (NSString *)databaseName;

@end
