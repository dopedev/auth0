//
//  AHAuth0ExampleViewController.m
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHAuth0ExampleViewController.h"
#import "AHAuth0LoginViewController.h"
#import "AHAuth0ExampleConfiguration.h"
#import "AHAuth0ExampleTheme.h"
#import <Masonry/Masonry.h>

@interface AHAuth0ExampleViewController () <AHAuth0LoginDelegate>

@property (nonatomic, strong) AHAuth0ExampleConfiguration *configuration;
@property (nonatomic, strong) AHAuth0ExampleTheme *theme;

@property (nonatomic, strong) UILabel *responseLabel;

@end

@implementation AHAuth0ExampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.theme = [AHAuth0ExampleTheme new];
    self.configuration = [AHAuth0ExampleConfiguration new];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"Press to show login" forState:UIControlStateNormal];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [button addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(20);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.mas_equalTo(44);
    }];
    
    self.responseLabel = [UILabel new];
    self.responseLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.responseLabel];
    
    [self.responseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(button.mas_bottom).with.offset(20);
        make.bottom.equalTo(self.view);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonPressed {
    AHAuth0LoginViewController *viewcontroller = [[AHAuth0LoginViewController alloc] initWithConfiguration:self.configuration];
    viewcontroller.theme = self.theme;
    viewcontroller.loginDelegate = self;
    
    [self presentViewController:viewcontroller animated:YES completion:nil];
}

#pragma mark - Login Delegate

- (void)loginDidCompleteSucessfully:(AHAuth0LoginViewController *)viewController response:(id)responseObject {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    self.responseLabel.text = @"Success!";
}

- (void)loginFailed:(AHAuth0LoginViewController *)viewController error:(NSError *)error {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    self.responseLabel.text = error.localizedDescription;
}

@end
