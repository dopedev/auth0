//
//  AHCustomButton.h
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  Custom button for styling and easy callbacks.
 */
@interface AHCustomButton : UIButton
/**
 *  Set this property to receive callbacks for UIControlEventTouchUpInside
 */
@property (nonatomic, strong) void (^buttonPressed)();

@end
