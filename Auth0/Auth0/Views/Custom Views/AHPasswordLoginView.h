//
//  AHPasswordLoginView.h
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHCustomTextField.h"
#import "AHCustomButton.h"
#import "AHAuth0LoginTheme.h"

@interface AHPasswordLoginView : UIView
@property (strong, nonatomic) UILabel *emailLabel;
@property (strong, nonatomic) AHCustomTextField *textField;
@property (strong, nonatomic) AHCustomButton *loginButton;
@property (strong, nonatomic) AHCustomButton *cancelButton;
@property (nonatomic, weak) id<AHAuth0LoginTheme> theme;

@end
