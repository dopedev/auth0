//
//  AHCustomButton.m
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHCustomButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation AHCustomButton

+ (instancetype)buttonWithType:(UIButtonType)buttonType {
    AHCustomButton *instance = [super buttonWithType: buttonType];
    
    if (instance) {
        [instance addTarget:instance action:@selector(tapped) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return instance;
}

- (void)awakeFromNib {
    [self addTarget:self action:@selector(tapped) forControlEvents:UIControlEventTouchUpInside];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = 5.0f;
}

- (void)tapped {
    
    if (self.buttonPressed != nil) {
        self.buttonPressed();
    }
    
}

@end
