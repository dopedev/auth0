//
//  AHPasswordLoginView.m
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHPasswordLoginView.h"
#import <Masonry/Masonry.h>
#import <libextobjc/EXTScope.h>

@implementation AHPasswordLoginView

- (instancetype)init {
    self = [super init];
    
    if (self) {
        @weakify(self)
     
        self.emailLabel = [UILabel new];
        self.emailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.emailLabel];
        
        [self.emailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            
            UIEdgeInsets padding = UIEdgeInsetsMake(15, 20, 15, -20);
            make.left.equalTo(self).with.offset(padding.left);
            make.right.equalTo(self).with.offset(padding.right);
            make.top.equalTo(self).with.offset(padding.top);
            
        }];
        
        self.textField = [AHCustomTextField new];
        self.textField.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.textField];
        
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
           @strongify(self)
            
            make.left.equalTo(self.emailLabel.mas_left);
            make.right.equalTo(self.emailLabel.mas_right);
            make.top.equalTo(self.emailLabel.mas_bottom).with.offset(15);
            make.height.mas_equalTo(44);
        }];
        
        self.loginButton = [AHCustomButton buttonWithType:UIButtonTypeCustom];
        [self.loginButton setTitle:@"Login" forState:UIControlStateNormal];
        self.loginButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.loginButton];
        
        [self.loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
           @strongify(self)
            
            make.left.equalTo(self.textField.mas_left);
            make.right.equalTo(self.textField.mas_right);
            make.top.equalTo(self.textField.mas_bottom).with.offset(15);
            make.height.mas_equalTo(44);
        }];
        
        self.cancelButton = [AHCustomButton buttonWithType:UIButtonTypeCustom];
        [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        self.cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.cancelButton];
        
        [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            
            make.left.equalTo(self.emailLabel.mas_left);
            make.right.equalTo(self.emailLabel.mas_right);
            make.top.equalTo(self.loginButton.mas_bottom).with.offset(5);
            make.height.mas_equalTo(44);
        }];
    };
    
    return self;
}

- (void)setTheme:(id<AHAuth0LoginTheme>)theme {
    _theme = theme;
    
    self.loginButton.backgroundColor = [theme primaryColor];
    [self.loginButton setTitleColor:[theme secondaryColor] forState:UIControlStateNormal];
    self.loginButton.titleLabel.font = [UIFont fontWithName:[theme fontName] size:14.0f];
    
    self.cancelButton.backgroundColor = [theme primaryColor];
    [self.cancelButton setTitleColor:[theme secondaryColor] forState:UIControlStateNormal];
    self.cancelButton.titleLabel.font = [UIFont fontWithName:[theme fontName] size:14.0f];
    
    self.textField.font = [UIFont fontWithName:[theme fontName] size:17.0f];
    self.emailLabel.font = [UIFont fontWithName:[theme fontName] size:17.0f];
}

@end
