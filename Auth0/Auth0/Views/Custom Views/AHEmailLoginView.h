//
//  AHEmailLoginView.h
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHCustomTextField.h"
#import "AHCustomButton.h"
#import "AHAuth0LoginTheme.h"

@interface AHEmailLoginView : UIView
@property (strong, nonatomic) AHCustomTextField *emailTextField;
@property (strong, nonatomic) AHCustomButton *passwordButton;
@property (strong, nonatomic) AHCustomButton *verificationButton;

@property (nonatomic, weak) id<AHAuth0LoginTheme> theme;

@end
