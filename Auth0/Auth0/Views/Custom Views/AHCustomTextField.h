//
//  AHCustomTextField.h
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AHCustomTextField : UITextField

@end
