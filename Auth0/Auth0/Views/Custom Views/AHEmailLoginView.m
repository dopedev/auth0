//
//  AHEmailLoginView.m
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHEmailLoginView.h"
#import <Masonry/Masonry.h>
#import <libextobjc/EXTScope.h>

@implementation AHEmailLoginView

- (instancetype)init {
    self = [super init];
    
    if (self) {
        @weakify(self)
        
        self.emailTextField = [AHCustomTextField new];
        self.emailTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.emailTextField.placeholder = @"Email";
        self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
        
        [self addSubview:self.emailTextField];
        
        [self.emailTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            
            UIEdgeInsets padding = UIEdgeInsetsMake(20, 20, 20, -20);
            make.left.equalTo(self).with.offset(padding.left);
            make.right.equalTo(self).with.offset(padding.right);
            make.top.equalTo(self).with.offset(padding.top);
            make.height.mas_equalTo(44);
            
        }];
        
        self.passwordButton = [AHCustomButton buttonWithType:UIButtonTypeCustom];
        self.passwordButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self.passwordButton setTitle:@"Enter password" forState:UIControlStateNormal];
        [self addSubview:self.passwordButton];
        
        [self.passwordButton mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
           
            make.left.equalTo(self.emailTextField.mas_left);
            make.right.equalTo(self.emailTextField.mas_right);
            make.top.equalTo(self.emailTextField.mas_bottom).with.offset(20);
            make.height.mas_equalTo(44);
        }];
        
        self.verificationButton = [AHCustomButton buttonWithType:UIButtonTypeCustom];
        self.verificationButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self.verificationButton setTitle:@"Get Verification Code" forState:UIControlStateNormal];
        [self addSubview:self.verificationButton];
        
        [self.verificationButton mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            
            make.left.equalTo(self.emailTextField.mas_left);
            make.right.equalTo(self.emailTextField.mas_right);
            make.top.equalTo(self.passwordButton.mas_bottom).with.offset(20);
            make.height.mas_equalTo(44);
        }];
        
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.backgroundColor = [UIColor clearColor];
}

- (void)setTheme:(id<AHAuth0LoginTheme>)theme {
    _theme = theme;
    
    self.passwordButton.backgroundColor = [theme primaryColor];
    [self.passwordButton setTitleColor:[theme secondaryColor] forState:UIControlStateNormal];
    self.passwordButton.titleLabel.font = [UIFont fontWithName:[theme fontName] size:14.0f];
    
    self.verificationButton.backgroundColor = [theme primaryColor];
    [self.verificationButton setTitleColor:[theme secondaryColor] forState:UIControlStateNormal];
    self.verificationButton.titleLabel.font = [UIFont fontWithName:[theme fontName] size:14.0f];
    
    self.emailTextField.font = [UIFont fontWithName:[theme fontName] size:17.0f];
}

@end
