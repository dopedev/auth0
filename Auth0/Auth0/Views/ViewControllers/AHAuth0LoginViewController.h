//
//  AHAuth0LoginViewController.h
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHAuth0LoginTheme.h"
#import "AHAuth0NetworkConfiguration.h"

@protocol AHAuth0LoginDelegate;

/**
 *  This view controller allows the user to login using email & password or passwordless login using email.
 Display this view controller modally to use for the Auth0 login process.
 */
@interface AHAuth0LoginViewController : UIViewController
/**
 *  Designated initalizer.
 *
 *  @param configuration Network configuration for Auth0
 *
 *  @return The new instance.
 */
- (instancetype _Nonnull)initWithConfiguration:(id<AHAuth0NetworkConfiguration> _Nonnull)configuration;
/**
 *  Set this delegate to receive callbacks from the login process.
 */
@property (nonatomic, weak, nullable) id<AHAuth0LoginDelegate> loginDelegate;
/**
 *  Set this protocol to customize the theme of the view controller.
 */
@property (nonatomic, weak, nullable) id<AHAuth0LoginTheme> theme;

@end

/**
 *  Defines the interface for a delegate to receive callbacks from the Auth0 process.
 */
@protocol AHAuth0LoginDelegate <NSObject>
/**
 *  Notifies the delegate that the login process completed sucessfully. The view controller will need to be manually dismissed.
 *
 *  @param viewController The view controller used for the login.
 *  @param responseObject The response object returned from Auth0.
 */
- (void)loginDidCompleteSucessfully:( AHAuth0LoginViewController * _Nonnull )viewController response:(id _Nonnull)responseObject;
/**
 *  Notifies the delegate that the login process failed. The view controller will need to be manually dismissed.
 *
 *  @param viewController The view controller used for the login.
 *  @param error          The error.
 */
- (void)loginFailed:(AHAuth0LoginViewController * _Nonnull)viewController error:(NSError * _Nonnull)error;

@end
