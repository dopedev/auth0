//
//  AHAuth0LoginViewController.m
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHAuth0LoginViewController.h"
#import <Masonry/Masonry.h>
#import "AHEmailLoginView.h"
#import "AHPasswordLoginView.h"
#import <libextobjc/EXTScope.h>
#import "AHAuth0Client.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface AHAuth0LoginViewController ()

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) AHEmailLoginView *emailView;
@property (nonatomic, strong) AHPasswordLoginView *passwordView;

@property (nonatomic, strong) AHAuth0Client *client;

@property (nonatomic, strong) id<AHAuth0NetworkConfiguration> networkConfiguration;

@end

@implementation AHAuth0LoginViewController

- (instancetype)initWithConfiguration:(id<AHAuth0NetworkConfiguration>)configuration {
    self = [super init];
    
    if (self) {
        NSParameterAssert(configuration);
        self.networkConfiguration = configuration;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
    [self setActions];
    
    self.client = [[AHAuth0Client alloc] initWithConfiguration:self.networkConfiguration];
    
    // keyboard events, adjust scrollview content size
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)setupViews {
    @weakify(self)
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.scrollView = [UIScrollView new];
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.scrollView];
    
    // hide keyboard if the user clicks anywhere on the screen.
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:tapGesture];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.edges.equalTo(self.view);
    }];
    
    self.logoImageView = [UIImageView new];
    self.logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.scrollView addSubview:self.logoImageView];
    
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        UIEdgeInsets padding = UIEdgeInsetsMake(75, 50, -50, -50);
        make.top.equalTo(self.scrollView).with.offset(padding.top);
        make.left.equalTo(self.view).with.offset(padding.left);
        make.right.equalTo(self.view).with.offset(padding.right);
        make.height.equalTo(self.view).multipliedBy(0.25);
    }];
    
    self.logoImageView.image = [self.theme logoImage];
    
    self.emailView = [AHEmailLoginView new];
    self.emailView.theme = self.theme;
    self.emailView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.scrollView addSubview:self.emailView];
    
    [self.emailView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.logoImageView.mas_bottom).with.offset(40);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.mas_equalTo(250);
    }];
    
    [self.emailView layoutIfNeeded];
    
    self.passwordView = [AHPasswordLoginView new];
    self.passwordView.theme = self.theme;
    self.passwordView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.scrollView addSubview:self.passwordView];
    
    [self.passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.logoImageView.mas_bottom).with.offset(40);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.mas_equalTo(250);
    }];
    self.passwordView.hidden = YES;
    [self.passwordView layoutIfNeeded];
    
    self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, CGRectGetMaxY(self.emailView.frame));
    
}

- (void)setActions {
    @weakify(self)
    
    
    self.emailView.passwordButton.buttonPressed = ^ {
        @strongify(self)
        
        // show password screen
        NSString *email = self.emailView.emailTextField.text;
        self.passwordView.emailLabel.text = email;
        self.passwordView.textField.placeholder = @"Enter Password";
        self.passwordView.textField.secureTextEntry = YES;
        [self showPasswordScreen];
    };
    
    self.emailView.verificationButton.buttonPressed = ^ {
        @strongify(self)
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD show];
        NSString *email = self.emailView.emailTextField.text;
        
        [self.client passwordlessLoginWithEmail:email
                                     completion:^(BOOL success, NSError * _Nullable error) {
                                         [SVProgressHUD dismiss];
                                         
                                         if (success) {
                                             self.passwordView.emailLabel.text = email;
                                             self.passwordView.textField.placeholder = @"Enter Verification Code";
                                             self.passwordView.textField.secureTextEntry = NO;
                                             // show password screen
                                             [self showPasswordScreen];

                                         } else {
                                             [self.loginDelegate loginFailed:self error:error];
                                         }
                                         
                                     }];
        
    };
    
    self.passwordView.loginButton.buttonPressed = ^ {
        @strongify(self)
        
        [SVProgressHUD show];
        [self.client loginWithEmail:self.passwordView.emailLabel.text
                        andPassword:self.passwordView.textField.text
                   verificationCode:!self.passwordView.textField.secureTextEntry
                            success:^(NSDictionary * _Nonnull response) {
                                [SVProgressHUD dismiss];
                                [self.loginDelegate loginDidCompleteSucessfully:self response:response];
                            } failure:^(NSError * _Nonnull error) {
                                [SVProgressHUD dismiss];
                                [self.loginDelegate loginFailed:self error:error];
                            }];
    };
    
    self.passwordView.cancelButton.buttonPressed = ^ {
      @strongify(self)
        [self showEmailScreen];
    };
}

- (void)showPasswordScreen {
    @weakify(self)
    [UIView animateWithDuration:0.50 animations:^{
        @strongify(self)
        self.emailView.hidden = YES;
        self.passwordView.hidden = NO;
    }];
}

- (void)showEmailScreen {
    @weakify(self)
    
    [UIView animateWithDuration:0.50 animations:^{
        @strongify(self)
        self.emailView.hidden = NO;
        self.passwordView.hidden = YES;
    }];
    
}

#pragma mark - Keyboard

- (void)keyboardShown:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardHidden:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

@end