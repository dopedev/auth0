//
//  AHAuth0NetworkConfiguration.h
//  Auth0
//
//  Created by Anthony Hoang on 4/26/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  Protocol for login configuration.
 */
@protocol AHAuth0NetworkConfiguration <NSObject>
/**
 *  The base URL used for Auth0
 *
 *  @return The URL used for Auth0
 */
- (NSString *)baseURL;
/**
 *  The client id used for requuest.
 *
 *  @return The client Id.
 */
- (NSString *)clientId;
/**
 *  The database name in your Auth0 account used for email/password authentication.
 *
 *  @return The database name.
 */
- (NSString *)databaseName;

@end
