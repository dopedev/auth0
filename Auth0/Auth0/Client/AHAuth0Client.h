//
//  AHAuth0Client.h
//  Auth0
//
//  Created by Anthony Hoang on 4/21/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AHAuth0NetworkConfiguration.h"
/**
 *  Network client used for HTTP requests to the Auth0 service.
 */
@interface AHAuth0Client : NSObject
/**
 *  Designated initalizer.
 *
 *  @param networkConfiguration Used to configure the network request. This cannot be null.
 *
 *  @return The new instance.
 */
- (_Nonnull instancetype)initWithConfiguration:(id<AHAuth0NetworkConfiguration> _Nonnull)networkConfiguration;
/**
 *  Login using email and password.
 *
 *  @param email            The email address.
 *  @param password         The password.
 *  @param verificationCode YES if the password is a verification code from the passwordless login process. NO if the password is the user's password.
 *  @param success          The success block invoked if the request completes successfully. The block sends the response.
 *  @param failure          The failure block invoked if the request fails. The block will return the error.
 */
- (void)loginWithEmail:(NSString * _Nonnull)email
           andPassword:(NSString * _Nonnull)password
      verificationCode:(BOOL)verificationCode
               success:(void (^ _Nullable)(NSDictionary * _Nonnull response))success
               failure:(void (^ _Nullable)(NSError * _Nonnull error))failure;
/**
 *  Passwordless login using email.
 *
 *  @param email      The email address.
 *  @param completion The completion block. The block will return YES if the request completed successfully, otherwise it will return NO and the error.
 */
- (void)passwordlessLoginWithEmail:(NSString * _Nonnull)email
                        completion:(void (^ _Nullable)(BOOL success, NSError * _Nullable error))completion;

@end
