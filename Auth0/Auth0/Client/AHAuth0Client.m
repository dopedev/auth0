//
//  AHAuth0Client.m
//  Auth0
//
//  Created by Anthony Hoang on 4/21/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHAuth0Client.h"

#import <AFNetworking/AFNetworking.h>

@interface AHAuth0Client ()
/**
 *  Used for network requests
 */
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
/**
 *  Used to configure the network.
 */
@property (nonatomic, strong) id<AHAuth0NetworkConfiguration> networkConfiguration;

@end

@implementation AHAuth0Client

- (instancetype)initWithConfiguration:(id<AHAuth0NetworkConfiguration>)networkConfiguration {
    self = [super init];
    
    if (self) {
        NSParameterAssert(@"must seet network configuration");
        self.networkConfiguration = networkConfiguration;
        self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:[self.networkConfiguration baseURL]]];
        
    }
    
    return self;
}

- (void)loginWithEmail:(NSString *)email
           andPassword:(NSString *)password
      verificationCode:(BOOL)verificationCode
               success:(void (^)(NSDictionary *))success
               failure:(void (^)(NSError *))failure {
    
    [self.sessionManager POST:@"oauth/ro"
                   parameters:@{@"client_id" : [self.networkConfiguration clientId],
                                @"username" : email,
                                @"password" : password,
                                @"connection" : verificationCode ? @"email" : [self.networkConfiguration databaseName],
                                @"grant_type": @"password",
                                @"scope" : @"openid"}
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          if (success) {
                              success(responseObject);
                          }
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          
                          
                          if (failure) {
                              failure(error);
                          }
                          
                      }];
}

- (void)passwordlessLoginWithEmail:(NSString *)email
                        completion:(void (^)(BOOL, NSError *))completion {
    
    [self.sessionManager POST:@"passwordless/start"
                   parameters:@{@"client_id" : [self.networkConfiguration clientId],
                                @"connection" : @"email",
                                @"send" : @"code",
                                @"email" : email}
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          if (completion) {
                              completion(YES, nil);
                          }
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          
                          if (completion) {
                              completion(NO, error);
                          }
                      }];
}

@end
